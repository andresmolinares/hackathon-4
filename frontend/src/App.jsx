import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import { Link, Route, Routes } from "react-router-dom";

import { AddCita } from "./components/Cita/AddCita";
import { AddEspecialidad } from "./components/Especialidad/AddEspecialidad";
import { AddHorario } from "./components/Horario/AddHorario";
import { AddMedico } from "./components/Medico/AddMedico";
import { AddMedicoEspecialidad } from "./components/MedicoEspecialidad/AddMedicoEspecialidad";
import { AddPaciente } from "./components/Paciente/AddPaciente";
import { Cita } from "./pages/Cita";
import { Especialidad } from "./pages/Especialidad";
import { Header } from "./components/Navbar/Header";
import { Home } from "./pages/Home";
import { Horario } from "./pages/Horario";
import Login from "./pages/login";
import { Medico } from "./pages/Medico";
import { MedicoEspecialidad } from "./pages/Medico_Especialidad";
import { Paciente } from "./pages/Paciente";
import { UpdateCita } from "./components/Cita/UpdateCita";
import { UpdateEspecialidad } from "./components/Especialidad/UpdateEspecialidad";
import { UpdateHorario } from "./components/Horario/UpdateHorario";
import { UpdateMedico } from "./components/Medico/UpdateMedico";
import { UpdateMedicoEspecialidad } from "./components/MedicoEspecialidad/UpdateMedicoEspecialidad";
import { UpdatePaciente } from "./components/Paciente/UpdatePaciente";

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/medicos" element={<Medico />} />
        <Route path="/medicos/agregar" element={<AddMedico />} />
        <Route path="/medicos/editar/:id/" element={<UpdateMedico />} />
        <Route path="/especialidades" element={<Especialidad />} />
        <Route path="/especialidades/agregar" element={<AddEspecialidad />} />
        <Route
          path="/especialidades/editar/:id/"
          element={<UpdateEspecialidad />}
        />
        <Route path="/pacientes" element={<Paciente />} />
        <Route path="/pacientes/agregar" element={<AddPaciente />} />
        <Route path="/pacientes/editar/:id/" element={<UpdatePaciente />} />
        <Route path="/citas" element={<Cita />} />
        <Route path="/citas/agregar" element={<AddCita />} />
        <Route path="/citas/editar/:id/" element={<UpdateCita />} />
        <Route path="/horarios" element={<Horario />} />
        <Route path="/horarios/agregar" element={<AddHorario />} />
        <Route path="/horarios/editar/:id/" element={<UpdateHorario />} />
        <Route path="/medico_especialidad" element={<MedicoEspecialidad />} />
        <Route
          path="/medico_especialidad/agregar"
          element={<AddMedicoEspecialidad />}
        />
        <Route
          path="/medico_especialidad/editar/:id/"
          element={<UpdateMedicoEspecialidad />}
        />
      </Routes>
    </div>
  );
}

export default App;
