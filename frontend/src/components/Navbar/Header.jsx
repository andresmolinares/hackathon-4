import { Link } from "react-router-dom";
export function Header() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <a className="navbar-brand" href="#">
          Hospital
        </a>
        <div className="collapse navbar-collapse" id="navbarText">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link className="nav-link" to="/">
                Home
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/medicos">
                Medicos
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/pacientes">
                Pacientes
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/citas">
                Citas
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/horarios">
                Horarios
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/especialidades">
                Especialidades
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/medico_especialidad">
                MedicoEspecialidad
              </Link>
            </li>
          </ul>
          <ul className="nav justify-content-end">
            <li className="nav-item">
              <Link className="nav-link" to="/login">
                Login
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
