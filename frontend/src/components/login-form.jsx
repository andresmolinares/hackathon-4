import { Field, Form, Formik } from "formik";

const LoginForm = ({ onSubmit, innerRef }) => {
  const incial_data = {
    username: "",
    password: "",
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };

  return (
    <Formik initialValues={incial_data} onSubmit={submit} innerRef={innerRef}>
      {({ isSubmitting }) => {
        return (
          <Form>
            <div className="form-group pb-3">
              <div style={{ textAlign: "left" }}>
                <label htmlFor="username">Username</label>
              </div>
              <Field
                className="form-control"
                id="username"
                type="text"
                name="username"
                placeholder="Enter your username"
              />
            </div>
            <div className="form-group pb-3">
              <div style={{ textAlign: "left" }}>
                <label htmlFor="pass">Password</label>
              </div>
              <Field
                className="form-control"
                id="pass"
                type="password"
                name="password"
                placeholder="Enter your password"
              />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default LoginForm;
