import { Button, Table } from "reactstrap";
import { Link, useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";

import axios from "axios";

const EspecialidadList = () => {
  const [especialidad, setEspecialidad] = useState([]);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  useEffect(() => {
    const fetchEspecialidad = async () => {
      const res = await axios.get(
        "http://localhost:8000/api/especialidades/",
        config
      );
      console.log(res.data);
      setEspecialidad(res.data.results);
    };

    fetchEspecialidad().catch((err) => console.log(err));
  }, []);

  function handleDelete(id) {
    const confirm = window.confirm("Estas seguro de eliminar?");
    if (confirm) {
      axios
        .delete(`http://localhost:8000/api/especialidades/${id}`, config)
        .then((res) => console.log(res.data))
        .catch((err) => console.log(err));
      window.location.replace("/especialidades");
    }
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h1>Lista de Especialidades</h1>
          <div className="form-group text-center p-2">
            <Link
              className="btn btn-success rounded-pill"
              to="/especialidades/agregar"
            >
              Crear
            </Link>
          </div>
          <hr />
          <Table>
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Activo</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {especialidad
                ? especialidad.map((especialidad) => (
                    <tr key={especialidad.id}>
                      <td>{especialidad.nombre}</td>
                      <td>{especialidad.descripcion}</td>
                      <td>{especialidad.activo ? "Si" : "No"}</td>
                      <td>
                        <Link to={`/especialidades/editar/${especialidad.id}/`}>
                          <Button className="btn btn-primary rounded-pill">
                            Editar
                          </Button>
                        </Link>

                        <Button
                          className="btn btn-danger rounded-pill"
                          onClick={() => handleDelete(especialidad.id)}
                        >
                          Delete
                        </Button>
                      </td>
                    </tr>
                  ))
                : "No hay registros"}
            </tbody>
          </Table>
        </div>
      </div>
    </div>
  );
};

export default EspecialidadList;
