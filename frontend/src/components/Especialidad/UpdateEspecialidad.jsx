import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import { useEffect, useState } from "react";

import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";

export function UpdateEspecialidad() {
  const [especialidad, setEspecialidad] = useState({
    nombre: "",
    descripcion: "",
  });
  const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };
  const navigate = useNavigate();
  const { id } = useParams();
  //   const [especialidad, setespecialidad] = useState({});

  function handleOnChange(event) {
    const { name, value } = event.target;
    value.activo == "on" ? true : false;
    setEspecialidad({ ...especialidad, [name]: value });
  }

  const loadEspecialidad = async () => {
    const res = await axios.get(
      `http://localhost:8000/api/especialidades/${id}`,
      config
    );
    setEspecialidad(res.data);
  };

  useEffect(() => {
    loadEspecialidad();
  }, []);

  function handleUpdate(event, id) {
    event.preventDefault();
    axios
      .put(
        `http://localhost:8000/api/especialidades/${id}/`,
        especialidad,
        config
      )
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
    navigate("/especialidades");
  }

  return (
    <div className="container mt-5">
      <Card>
        <CardHeader tag="h5">Editar Especialidad</CardHeader>
        <CardBody>
          {especialidad ? (
            <Form
              onSubmit={(e) => {
                handleUpdate(e, id);
              }}
            >
              <FormGroup>
                <Label for="nombre">Nombre</Label>
                <Input
                  id="nombre"
                  name="nombre"
                  placeholder="nombre"
                  type="text"
                  value={especialidad.nombre}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="descripcion">Descripcion</Label>
                <Input
                  id="descripcion"
                  name="descripcion"
                  placeholder="descripcion"
                  type="text"
                  value={especialidad.descripcion}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="activo">Activo</Label>
                <Input
                  id="activo"
                  name="activo"
                  type="checkbox"
                  checked={especialidad.activo ? "checked" : ""}
                  onChange={handleOnChange}
                />
              </FormGroup>

              <Button>Submit</Button>
            </Form>
          ) : (
            "Loading..."
          )}
        </CardBody>
      </Card>
    </div>
  );
}
