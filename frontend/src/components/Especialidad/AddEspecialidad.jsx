import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";

import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useState } from "react";

export function AddEspecialidad() {
  // const [especialidad, setEspecialidad] = useState(null);
  const navigate = useNavigate();
  const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };

  function handleSubmit(event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    const values = Object.fromEntries(formData.entries());
    values.activo = values.activo == "on" ? true : false;
    axios
      .post("http://localhost:8000/api/especialidades/", values, config)
      .then((res) => console.log(res.data))
      .catch((err) => console.log(err));
    navigate("/especialidades");
  }

  return (
    <div className="container mt-5">
      <Card>
        <CardHeader tag="h5">Agregar Especialidad</CardHeader>
        <CardBody>
          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label for="nombre">Nombre</Label>
              <Input
                id="nombre"
                name="nombre"
                placeholder="nombre"
                type="text"
              />
            </FormGroup>
            <FormGroup>
              <Label for="descripcion">Descripcion</Label>
              <Input
                id="descripcion"
                name="descripcion"
                placeholder="descripcion"
                type="text"
              />
            </FormGroup>
            <FormGroup>
              <Label for="activo">Activo</Label>
              <Input id="activo" name="activo" type="checkbox" />
            </FormGroup>

            <Button>Submit</Button>
          </Form>
        </CardBody>
      </Card>
    </div>
  );
}
