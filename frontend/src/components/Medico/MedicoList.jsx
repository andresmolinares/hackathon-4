import { Button, Table } from "reactstrap";
import { Link, useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";

import axios from "axios";

const MedicoList = () => {
  const [medico, setMedico] = useState([]);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  useEffect(() => {
    const fetchMedico = async () => {
      const res = await axios.get("http://localhost:8000/api/medicos/", config);
      console.log(res.data);
      setMedico(res.data.results);
    };

    fetchMedico().catch((err) => console.log(err));
  }, []);

  function handleDelete(id) {
    const confirm = window.confirm("Estas seguro de eliminar?");
    if (confirm) {
      axios
        .delete(`http://localhost:8000/api/medicos/${id}`, config)
        .then((res) => console.log(res.data))
        .catch((err) => console.log(err));
      window.location.replace("/medicos");
    }
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h1>Lista de medicos</h1>
          <div className="form-group text-center p-2">
            <Link
              className="btn btn-success rounded-pill"
              to="/medicos/agregar"
            >
              Crear
            </Link>
          </div>
          <hr />
          <Table>
            <thead>
              <tr>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>DNI</th>
                <th>Direccion</th>
                <th>Correo</th>
                <th>Telefono</th>
                <th>Sexo</th>
                <th>Numero Colegiatura</th>
                <th>Fecha de nacimiento</th>
                <th>Activo</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {medico
                ? medico.map((medico) => (
                    <tr key={medico.id}>
                      <td>{medico.nombres}</td>
                      <td>{medico.apellidos}</td>
                      <td>{medico.dni}</td>
                      <td>{medico.direccion}</td>
                      <td>{medico.correo}</td>
                      <td>{medico.telefono}</td>
                      <td>{medico.sexo}</td>
                      <td>{medico.numcolegiatura}</td>
                      <td>{medico.fecha_nacimiento}</td>
                      <td>{medico.activo ? "Si" : "No"}</td>
                      <td>
                        <Link to={`/medicos/editar/${medico.id}/`}>
                          <Button className="btn btn-primary rounded-pill">
                            Editar
                          </Button>
                        </Link>

                        <Button
                          className="btn btn-danger rounded-pill"
                          onClick={() => handleDelete(medico.id)}
                        >
                          Delete
                        </Button>
                      </td>
                    </tr>
                  ))
                : "No hay registros"}
            </tbody>
          </Table>
        </div>
      </div>
    </div>
  );
};

export default MedicoList;
