import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import { useEffect, useState } from "react";

import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";

export function UpdateMedico() {
  const [medico, setMedico] = useState({
    nombres: "",
    apellidos: "",
    dni: "",
    direccion: "",
    correo: "",
    telefono: "",
    sexo: "",
    numcolegiatura: "",
    fecha_nacimiento: "",
  });
  const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };
  const navigate = useNavigate();
  const { id } = useParams();

  function handleOnChange(event) {
    const { name, value } = event.target;
    value.activo == "on" ? true : false;
    setMedico({ ...medico, [name]: value });
  }

  const loadMedico = async () => {
    const res = await axios.get(
      `http://localhost:8000/api/medicos/${id}`,
      config
    );
    setMedico(res.data);
  };

  useEffect(() => {
    loadMedico();
  }, []);

  function handleUpdate(event, id) {
    event.preventDefault();
    axios
      .put(`http://localhost:8000/api/medicos/${id}/`, medico, config)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
    navigate("/medicos");
  }

  return (
    <div className="container mt-5">
      <Card>
        <CardHeader tag="h5">Editar Medico</CardHeader>
        <CardBody>
          {medico ? (
            <Form
              onSubmit={(e) => {
                handleUpdate(e, id);
              }}
            >
              <FormGroup>
                <Label for="nombres">Nombres</Label>
                <Input
                  id="nombres"
                  name="nombres"
                  placeholder="nombre"
                  type="text"
                  value={medico.nombres}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="apellidos">Apellidos</Label>
                <Input
                  id="apellidos"
                  name="apellidos"
                  placeholder="apellidos"
                  type="text"
                  value={medico.apellidos}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="dni">DNI</Label>
                <Input
                  id="dni"
                  name="dni"
                  placeholder="dni"
                  type="text"
                  value={medico.dni}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="direccion">Direccion</Label>
                <Input
                  id="direccion"
                  name="direccion"
                  placeholder="direccion"
                  type="textarea"
                  value={medico.direccion}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="correo">Correo</Label>
                <Input
                  id="correo"
                  name="correo"
                  placeholder="correo"
                  type="email"
                  value={medico.correo}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="telefono">Telefono</Label>
                <Input
                  id="telefono"
                  name="telefono"
                  placeholder="telefono"
                  type="tel"
                  value={medico.telefono}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="sexo">Sexo</Label>
                <Input
                  id="sexo"
                  name="sexo"
                  placeholder="sexo"
                  type="text"
                  value={medico.sexo}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="numcolegiatura">Numero de colegiatura</Label>
                <Input
                  id="numcolegiatura"
                  name="numcolegiatura"
                  placeholder="numcolegiatura"
                  type="number"
                  value={medico.numcolegiatura}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="fecha_nacimiento">Fecha de nacimiento</Label>
                <Input
                  id="fecha_nacimiento"
                  name="fecha_nacimiento"
                  placeholder="fecha_nacimiento"
                  type="date"
                  value={medico.fecha_nacimiento}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="activo">Activo</Label>
                <Input
                  id="activo"
                  name="activo"
                  type="checkbox"
                  checked={medico.activo ? "checked" : ""}
                  onChange={handleOnChange}
                />
              </FormGroup>

              <Button>Submit</Button>
            </Form>
          ) : (
            "Loading..."
          )}
        </CardBody>
      </Card>
    </div>
  );
}
