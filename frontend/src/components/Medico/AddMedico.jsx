import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";

import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useState } from "react";

export function AddMedico() {
  const navigate = useNavigate();
  const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };

  function handleSubmit(event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    const values = Object.fromEntries(formData.entries());
    values.activo = values.activo == "on" ? true : false;
    axios
      .post("http://localhost:8000/api/medicos/", values, config)
      .then((res) => console.log(res.data))
      .catch((err) => console.log(err));
    navigate("/medicos");
  }

  return (
    <div className="container mt-5">
      <Card>
        <CardHeader tag="h5">Agregar Medico</CardHeader>
        <CardBody>
          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label for="nombres">Nombres</Label>
              <Input
                id="nombres"
                name="nombres"
                placeholder="nombre"
                type="text"
              />
            </FormGroup>
            <FormGroup>
              <Label for="apellidos">Apellidos</Label>
              <Input
                id="apellidos"
                name="apellidos"
                placeholder="apellidos"
                type="text"
              />
            </FormGroup>
            <FormGroup>
              <Label for="dni">DNI</Label>
              <Input id="dni" name="dni" placeholder="dni" type="text" />
            </FormGroup>
            <FormGroup>
              <Label for="direccion">Direccion</Label>
              <Input
                id="direccion"
                name="direccion"
                placeholder="direccion"
                type="textarea"
              />
            </FormGroup>
            <FormGroup>
              <Label for="correo">Correo</Label>
              <Input
                id="correo"
                name="correo"
                placeholder="correo"
                type="email"
              />
            </FormGroup>
            <FormGroup>
              <Label for="telefono">Telefono</Label>
              <Input
                id="telefono"
                name="telefono"
                placeholder="telefono"
                type="tel"
              />
            </FormGroup>
            <FormGroup>
              <Label for="sexo">Sexo</Label>
              <Input id="sexo" name="sexo" placeholder="sexo" type="text" />
            </FormGroup>
            <FormGroup>
              <Label for="numcolegiatura">Numero de colegiatura</Label>
              <Input
                id="numcolegiatura"
                name="numcolegiatura"
                placeholder="numcolegiatura"
                type="number"
              />
            </FormGroup>
            <FormGroup>
              <Label for="fecha_nacimiento">Fecha de nacimiento</Label>
              <Input
                id="fecha_nacimiento"
                name="fecha_nacimiento"
                placeholder="fecha_nacimiento"
                type="date"
              />
            </FormGroup>
            <FormGroup>
              <Label for="activo">Activo</Label>
              <Input id="activo" name="activo" type="checkbox" />
            </FormGroup>

            <Button>Submit</Button>
          </Form>
        </CardBody>
      </Card>
    </div>
  );
}
