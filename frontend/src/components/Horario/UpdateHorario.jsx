import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import { useEffect, useState } from "react";

import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";

export function UpdateHorario() {
  const [medico, setMedico] = useState([]);
  const [horario, setHorario] = useState({
    id_medico: "",
    fecha_atencion: "",
    hora_inicio: "",
    hora_fin: "",
  });
  const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };
  const navigate = useNavigate();
  const { id } = useParams();

  function handleOnChange(event) {
    const { name, value } = event.target;
    value.activo == "on" ? true : false;
    value.estado == "on" ? true : false;
    setHorario({ ...horario, [name]: value });
  }

  const loadHorario = async () => {
    const res = await axios.get(
      `http://localhost:8000/api/horarios/${id}`,
      config
    );
    setHorario(res.data);
  };

  useEffect(() => {
    loadHorario();
  }, []);
  useEffect(() => {
    axios
      .get("http://localhost:8000/api/medicos/", config)
      .then((response) => {
        setMedico(response.data.results);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  function handleUpdate(event, id) {
    event.preventDefault();
    axios
      .put(`http://localhost:8000/api/horarios/${id}/`, horario, config)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
    navigate("/horarios");
  }

  return (
    <div className="container mt-5">
      <Card>
        <CardHeader tag="h5">Editar Horario</CardHeader>
        <CardBody>
          {horario ? (
            <Form
              onSubmit={(e) => {
                handleUpdate(e, id);
              }}
            >
              <FormGroup>
                <Label for="id_medico">Medico</Label>
                <Input
                  id="id_medico"
                  name="id_medico"
                  placeholder="id_medico"
                  type="select"
                  value={horario.id_medico}
                  onChange={handleOnChange}
                >
                  <option value="">Seleccione uno...</option>

                  {medico.map((medico) => (
                    <option key={medico.id} value={medico.id}>
                      {medico.nombres} {medico.apellidos}
                    </option>
                  ))}
                </Input>
              </FormGroup>

              <FormGroup>
                <Label for="fecha_atencion">Fecha</Label>
                <Input
                  id="fecha_atencion"
                  name="fecha_atencion"
                  placeholder="fecha_atencion"
                  type="date"
                  value={horario.fecha_atencion}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="inicio_atencion">Hora inicio</Label>
                <Input
                  id="inicio_atencion"
                  name="inicio_atencion"
                  placeholder="inicio_atencion"
                  type="time"
                  value={horario.inicio_atencion}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="fin_atencion">Hora fin</Label>
                <Input
                  id="fin_atencion"
                  name="fin_atencion"
                  placeholder="fin_atencion"
                  type="time"
                  value={horario.fin_atencion}
                  onChange={handleOnChange}
                />
              </FormGroup>

              <FormGroup>
                <Label for="activo">Activo</Label>
                <Input
                  id="activo"
                  name="activo"
                  type="checkbox"
                  checked={horario.activo ? "checked" : ""}
                  onChange={handleOnChange}
                />
              </FormGroup>

              <Button>Submit</Button>
            </Form>
          ) : (
            "Loading..."
          )}
        </CardBody>
      </Card>
    </div>
  );
}
