import { Button, Table } from "reactstrap";
import { Link, useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";

import axios from "axios";

const HorarioList = () => {
  const [horario, setHorario] = useState([]);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  useEffect(() => {
    const fetchHorario = async () => {
      const res = await axios.get(
        "http://localhost:8000/api/horarios/",
        config
      );
      console.log(res.data);
      setHorario(res.data.results);
    };

    fetchHorario().catch((err) => console.log(err));
  }, []);

  function handleDelete(id) {
    const confirm = window.confirm("Estas seguro de eliminar?");
    if (confirm) {
      axios
        .delete(`http://localhost:8000/api/horarios/${id}`, config)
        .then((res) => console.log(res.data))
        .catch((err) => console.log(err));
      window.location.replace("/horarios");
    }
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h1>Lista de horarios</h1>
          <div className="form-group text-center p-2">
            <Link
              className="btn btn-success rounded-pill"
              to="/horarios/agregar"
            >
              Crear
            </Link>
          </div>
          <hr />
          <Table>
            <thead>
              <tr>
                <th>Medico</th>
                <th>Fecha de atencion</th>
                <th>Hora inicio</th>
                <th>Hora fin</th>
                <th>Activo</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {horario
                ? horario.map((horario) => (
                    <tr key={horario.id}>
                      <td>{horario.id_medico}</td>
                      <td>{horario.fecha_atencion}</td>
                      <td>{horario.inicio_atencion}</td>
                      <td>{horario.fin_atencion}</td>
                      <td>{horario.activo ? "Si" : "No"}</td>
                      <td>
                        <Link to={`/horarios/editar/${horario.id}/`}>
                          <Button className="btn btn-primary rounded-pill">
                            Editar
                          </Button>
                        </Link>

                        <Button
                          className="btn btn-danger rounded-pill"
                          onClick={() => handleDelete(horario.id)}
                        >
                          Delete
                        </Button>
                      </td>
                    </tr>
                  ))
                : "No hay registros"}
            </tbody>
          </Table>
        </div>
      </div>
    </div>
  );
};

export default HorarioList;
