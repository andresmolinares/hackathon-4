import { Button, Table } from "reactstrap";
import { Link, useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";

import axios from "axios";

const CitaList = () => {
  const [cita, setCita] = useState([]);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  useEffect(() => {
    const fetchCita = async () => {
      const res = await axios.get("http://localhost:8000/api/citas/", config);
      console.log(res.data);
      setCita(res.data.results);
    };

    fetchCita().catch((err) => console.log(err));
  }, []);

  function handleDelete(id) {
    const confirm = window.confirm("Estas seguro de eliminar?");
    if (confirm) {
      axios
        .delete(`http://localhost:8000/api/citas/${id}`, config)
        .then((res) => console.log(res.data))
        .catch((err) => console.log(err));
      window.location.replace("/citas");
    }
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h1>Lista de citas</h1>
          <div className="form-group text-center p-2">
            <Link className="btn btn-success rounded-pill" to="/citas/agregar">
              Crear
            </Link>
          </div>
          <hr />
          <Table>
            <thead>
              <tr>
                <th>Medico</th>
                <th>Paciente</th>
                <th>Fecha de atencion</th>
                <th>Hora inicio</th>
                <th>Hora fin</th>
                <th>Estado</th>
                <th>Observaciones</th>
                <th>Activo</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {cita
                ? cita.map((cita) => (
                    <tr key={cita.id}>
                      <td>{cita.id_medico}</td>
                      <td>{cita.id_paciente}</td>
                      <td>{cita.fecha_atencion}</td>
                      <td>{cita.inicio_atencion}</td>
                      <td>{cita.fin_atencion}</td>
                      <td>{cita.estado ? "Activo" : "Inactivo"}</td>
                      <td>{cita.observaciones}</td>
                      <td>{cita.activo ? "Si" : "No"}</td>
                      <td>
                        <Link to={`/citas/editar/${cita.id}/`}>
                          <Button className="btn btn-primary rounded-pill">
                            Editar
                          </Button>
                        </Link>

                        <Button
                          className="btn btn-danger rounded-pill"
                          onClick={() => handleDelete(cita.id)}
                        >
                          Delete
                        </Button>
                      </td>
                    </tr>
                  ))
                : "No hay registros"}
            </tbody>
          </Table>
        </div>
      </div>
    </div>
  );
};

export default CitaList;
