import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import { useEffect, useState } from "react";

import axios from "axios";
import { useNavigate } from "react-router-dom";

export function AddCita() {
  const [medico, setMedico] = useState([]);
  const [paciente, setPaciente] = useState([]);
  const navigate = useNavigate();
  const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };

  useEffect(() => {
    axios
      .get("http://localhost:8000/api/medicos/", config)
      .then((response) => {
        setMedico(response.data.results);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  useEffect(() => {
    axios
      .get("http://localhost:8000/api/pacientes/", config)
      .then((response) => {
        setPaciente(response.data.results);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  function handleSubmit(event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    const values = Object.fromEntries(formData.entries());
    values.activo = values.activo == "on" ? true : false;
    values.estado = values.estado == "on" ? true : false;
    axios
      .post("http://localhost:8000/api/citas/", values, config)
      .then((res) => console.log(res.data))
      .catch((err) => console.log(err));
    navigate("/citas");
  }

  return (
    <div className="container mt-5">
      <Card>
        <CardHeader tag="h5">Agendar cita</CardHeader>
        <CardBody>
          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label for="id_medico">Medico</Label>
              <Input
                id="id_medico"
                name="id_medico"
                placeholder="id_medico"
                type="select"
              >
                <option value="">Seleccione uno...</option>

                {medico.map((medico) => (
                  <option key={medico.id} value={medico.id}>
                    {medico.nombres} {medico.apellidos}
                  </option>
                ))}
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="id_paciente">Paciente</Label>
              <Input
                id="id_paciente"
                name="id_paciente"
                placeholder="id_paciente"
                type="select"
              >
                <option value="">Seleccione uno...</option>
                {paciente.map((paciente) => (
                  <option key={paciente.id} value={paciente.id}>
                    {paciente.nombres} {paciente.apellidos}
                  </option>
                ))}
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="fecha_atencion">Fecha</Label>
              <Input
                id="fecha_atencion"
                name="fecha_atencion"
                placeholder="fecha_atencion"
                type="date"
              />
            </FormGroup>
            <FormGroup>
              <Label for="inicio_atencion">Hora inicio</Label>
              <Input
                id="inicio_atencion"
                name="inicio_atencion"
                placeholder="inicio_atencion"
                type="time"
              />
            </FormGroup>
            <FormGroup>
              <Label for="fin_atencion">Hora fin</Label>
              <Input
                id="fin_atencion"
                name="fin_atencion"
                placeholder="fin_atencion"
                type="time"
              />
            </FormGroup>
            <FormGroup>
              <Label for="observaciones">Observaciones</Label>
              <Input
                id="observaciones"
                name="observaciones"
                placeholder="observaciones"
                type="textarea"
              />
            </FormGroup>
            <FormGroup>
              <Label for="estado">Estado</Label>
              <Input id="estado" name="estado" type="checkbox" />
            </FormGroup>
            <FormGroup>
              <Label for="activo">Activo</Label>
              <Input id="activo" name="activo" type="checkbox" />
            </FormGroup>

            <Button>Submit</Button>
          </Form>
        </CardBody>
      </Card>
    </div>
  );
}
