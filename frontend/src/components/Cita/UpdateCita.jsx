import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import { useEffect, useState } from "react";

import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";

export function UpdateCita() {
  const [medico, setMedico] = useState([]);
  const [paciente, setPaciente] = useState([]);
  const [cita, setCita] = useState({
    id_medico: "",
    id_paciente: "",
    fecha_atencion: "",
    hora_inicio: "",
    hora_fin: "",
    observaciones: "",
  });
  const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };
  const navigate = useNavigate();
  const { id } = useParams();

  function handleOnChange(event) {
    const { name, value } = event.target;
    value.activo == "on" ? true : false;
    value.estado == "on" ? true : false;
    setCita({ ...cita, [name]: value });
  }

  const loadCita = async () => {
    const res = await axios.get(
      `http://localhost:8000/api/citas/${id}`,
      config
    );
    setCita(res.data);
  };

  useEffect(() => {
    loadCita();
  }, []);
  useEffect(() => {
    axios
      .get("http://localhost:8000/api/medicos/", config)
      .then((response) => {
        setMedico(response.data.results);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  useEffect(() => {
    axios
      .get("http://localhost:8000/api/pacientes/", config)
      .then((response) => {
        setPaciente(response.data.results);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  function handleUpdate(event, id) {
    event.preventDefault();
    axios
      .put(`http://localhost:8000/api/citas/${id}/`, cita, config)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
    navigate("/citas");
  }

  return (
    <div className="container mt-5">
      <Card>
        <CardHeader tag="h5">Editar Cita</CardHeader>
        <CardBody>
          {cita ? (
            <Form
              onSubmit={(e) => {
                handleUpdate(e, id);
              }}
            >
              <FormGroup>
                <Label for="id_medico">Medico</Label>
                <Input
                  id="id_medico"
                  name="id_medico"
                  placeholder="id_medico"
                  type="select"
                  value={cita.id_medico}
                  onChange={handleOnChange}
                >
                  <option value="">Seleccione uno...</option>

                  {medico.map((medico) => (
                    <option key={medico.id} value={medico.id}>
                      {medico.nombres} {medico.apellidos}
                    </option>
                  ))}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label for="id_paciente">Paciente</Label>
                <Input
                  id="id_paciente"
                  name="id_paciente"
                  placeholder="id_paciente"
                  type="select"
                  value={cita.id_paciente}
                  onChange={handleOnChange}
                >
                  <option value="">Seleccione uno...</option>
                  {paciente.map((paciente) => (
                    <option key={paciente.id} value={paciente.id}>
                      {paciente.nombres} {paciente.apellidos}
                    </option>
                  ))}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label for="fecha_atencion">Fecha</Label>
                <Input
                  id="fecha_atencion"
                  name="fecha_atencion"
                  placeholder="fecha_atencion"
                  type="date"
                  value={cita.fecha_atencion}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="inicio_atencion">Hora inicio</Label>
                <Input
                  id="inicio_atencion"
                  name="inicio_atencion"
                  placeholder="inicio_atencion"
                  type="time"
                  value={cita.inicio_atencion}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="fin_atencion">Hora fin</Label>
                <Input
                  id="fin_atencion"
                  name="fin_atencion"
                  placeholder="fin_atencion"
                  type="time"
                  value={cita.fin_atencion}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="observaciones">Observaciones</Label>
                <Input
                  id="observaciones"
                  name="observaciones"
                  placeholder="observaciones"
                  type="textarea"
                  value={cita.observaciones}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="estado">Estado</Label>
                <Input
                  id="estado"
                  name="estado"
                  type="checkbox"
                  checked={cita.estado ? "checked" : ""}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="activo">Activo</Label>
                <Input
                  id="activo"
                  name="activo"
                  type="checkbox"
                  checked={cita.activo ? "checked" : ""}
                  onChange={handleOnChange}
                />
              </FormGroup>

              <Button>Submit</Button>
            </Form>
          ) : (
            "Loading..."
          )}
        </CardBody>
      </Card>
    </div>
  );
}
