import { Button, Table } from "reactstrap";
import { Link, useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";

import axios from "axios";

const MedicoEspecialidadList = () => {
  const [medicoEspecialidad, setMedicoEspecialidad] = useState([]);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  useEffect(() => {
    const fetchMedicoEspecialidad = async () => {
      const res = await axios.get(
        "http://localhost:8000/api/medicos_especialidades/",
        config
      );
      console.log(res.data);
      setMedicoEspecialidad(res.data.results);
    };

    fetchMedicoEspecialidad().catch((err) => console.log(err));
  }, []);

  function handleDelete(id) {
    const confirm = window.confirm("Estas seguro de eliminar?");
    if (confirm) {
      axios
        .delete(
          `http://localhost:8000/api/medicos_especialidades/${id}`,
          config
        )
        .then((res) => console.log(res.data))
        .catch((err) => console.log(err));
      window.location.replace("/medico_especialidad");
    }
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h1>Lista de MedicoEspecialidads</h1>
          <div className="form-group text-center p-2">
            <Link
              className="btn btn-success rounded-pill"
              to="/medico_especialidad/agregar"
            >
              Crear
            </Link>
          </div>
          <hr />
          <Table>
            <thead>
              <tr>
                <th>Medico</th>
                <th>Especialidad</th>
                <th>Activo</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {medicoEspecialidad
                ? medicoEspecialidad.map((medicoEspecialidad) => (
                    <tr key={medicoEspecialidad.id}>
                      <td>{medicoEspecialidad.id_medico}</td>
                      <td>{medicoEspecialidad.id_especialidad}</td>
                      <td>{medicoEspecialidad.activo ? "Si" : "No"}</td>
                      <td>
                        <Link
                          to={`/medico_especialidad/editar/${medicoEspecialidad.id}/`}
                        >
                          <Button className="btn btn-primary rounded-pill">
                            Editar
                          </Button>
                        </Link>

                        <Button
                          className="btn btn-danger rounded-pill"
                          onClick={() => handleDelete(medicoEspecialidad.id)}
                        >
                          Delete
                        </Button>
                      </td>
                    </tr>
                  ))
                : "No hay registros"}
            </tbody>
          </Table>
        </div>
      </div>
    </div>
  );
};

export default MedicoEspecialidadList;
