import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import { useEffect, useState } from "react";

import axios from "axios";
import { useNavigate } from "react-router-dom";

export function AddMedicoEspecialidad() {
  const [medico, setMedico] = useState([]);
  const [especialidad, setEspecialidad] = useState([]);
  const navigate = useNavigate();
  const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };

  useEffect(() => {
    axios
      .get("http://localhost:8000/api/medicos/", config)
      .then((response) => {
        setMedico(response.data.results);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  useEffect(() => {
    axios
      .get("http://localhost:8000/api/especialidades/", config)
      .then((response) => {
        setEspecialidad(response.data.results);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  function handleSubmit(event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    const values = Object.fromEntries(formData.entries());
    values.activo = values.activo == "on" ? true : false;
    axios
      .post("http://localhost:8000/api/medicos_especialidades/", values, config)
      .then((res) => console.log(res.data))
      .catch((err) => console.log(err));
    navigate("/medico_especialidad");
  }

  return (
    <div className="container mt-5">
      <Card>
        <CardHeader tag="h5">Agregar especialidad a medico</CardHeader>
        <CardBody>
          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label for="id_medico">Medico</Label>
              <Input
                id="id_medico"
                name="id_medico"
                placeholder="id_medico"
                type="select"
              >
                <option value="">Seleccione uno...</option>

                {medico.map((medico) => (
                  <option key={medico.id} value={medico.id}>
                    {medico.nombres} {medico.apellidos}
                  </option>
                ))}
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="id_especialidad">Especialidad</Label>
              <Input
                id="id_especialidad"
                name="id_especialidad"
                placeholder="id_especialidad"
                type="select"
              >
                <option value="">Seleccione uno...</option>
                {especialidad.map((especialidad) => (
                  <option key={especialidad.id} value={especialidad.id}>
                    {especialidad.nombre}
                  </option>
                ))}
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="activo">Activo</Label>
              <Input id="activo" name="activo" type="checkbox" />
            </FormGroup>

            <Button>Submit</Button>
          </Form>
        </CardBody>
      </Card>
    </div>
  );
}
