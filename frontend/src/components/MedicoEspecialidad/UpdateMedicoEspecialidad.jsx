import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import { useEffect, useState } from "react";

import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";

export function UpdateMedicoEspecialidad() {
  const [medico, setMedico] = useState([]);
  const [especialidad, setEspecialidad] = useState([]);
  const [medicoEspecialidad, setMedicoEspecialidad] = useState({
    id_medico: "",
    id_especialidad: "",
  });
  const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };
  const navigate = useNavigate();
  const { id } = useParams();

  function handleOnChange(event) {
    const { name, value } = event.target;
    value.activo == "on" ? true : false;
    setMedicoEspecialidad({ ...medicoEspecialidad, [name]: value });
  }

  const loadMedicoEspecialidad = async () => {
    const res = await axios.get(
      `http://localhost:8000/api/medicos_especialidades/${id}`,
      config
    );
    setMedicoEspecialidad(res.data);
  };

  useEffect(() => {
    loadMedicoEspecialidad();
  }, []);
  useEffect(() => {
    axios
      .get("http://localhost:8000/api/medicos/", config)
      .then((response) => {
        setMedico(response.data.results);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  useEffect(() => {
    axios
      .get("http://localhost:8000/api/especialidades/", config)
      .then((response) => {
        setEspecialidad(response.data.results);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  function handleUpdate(event, id) {
    event.preventDefault();
    axios
      .put(
        `http://localhost:8000/api/medicos_especialidades/${id}/`,
        medicoEspecialidad,
        config
      )
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
    navigate("/medico_especialidad");
  }

  return (
    <div className="container mt-5">
      <Card>
        <CardHeader tag="h5">Editar</CardHeader>
        <CardBody>
          {medicoEspecialidad ? (
            <Form
              onSubmit={(e) => {
                handleUpdate(e, id);
              }}
            >
              <FormGroup>
                <Label for="id_medico">Medico</Label>
                <Input
                  id="id_medico"
                  name="id_medico"
                  placeholder="id_medico"
                  type="select"
                  value={medicoEspecialidad.id_medico}
                  onChange={handleOnChange}
                >
                  <option value="">Seleccione uno...</option>

                  {medico.map((medico) => (
                    <option key={medico.id} value={medico.id}>
                      {medico.nombres} {medico.apellidos}
                    </option>
                  ))}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label for="id_especialidad">Especialidad</Label>
                <Input
                  id="id_especialidad"
                  name="id_especialidad"
                  placeholder="id_especialidad"
                  type="select"
                  value={medicoEspecialidad.id_especialidad}
                  onChange={handleOnChange}
                >
                  <option value="">Seleccione uno...</option>

                  {especialidad.map((especialidad) => (
                    <option key={especialidad.id} value={especialidad.id}>
                      {especialidad.nombre}
                    </option>
                  ))}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label for="activo">Activo</Label>
                <Input
                  id="activo"
                  name="activo"
                  type="checkbox"
                  checked={medicoEspecialidad.activo ? "checked" : ""}
                  onChange={handleOnChange}
                />
              </FormGroup>

              <Button>Submit</Button>
            </Form>
          ) : (
            "Loading..."
          )}
        </CardBody>
      </Card>
    </div>
  );
}
