import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import { useEffect, useState } from "react";

import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";

export function UpdatePaciente() {
  const [paciente, setPaciente] = useState({
    nombres: "",
    apellidos: "",
    dni: "",
    direccion: "",
    telefono: "",
    sexo: "",
    fecha_nacimiento: "",
  });
  const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };
  const navigate = useNavigate();
  const { id } = useParams();

  function handleOnChange(event) {
    const { name, value } = event.target;
    value.activo == "on" ? true : false;
    setPaciente({ ...paciente, [name]: value });
  }

  const loadPaciente = async () => {
    const res = await axios.get(
      `http://localhost:8000/api/pacientes/${id}`,
      config
    );
    setPaciente(res.data);
  };

  useEffect(() => {
    loadPaciente();
  }, []);

  function handleUpdate(event, id) {
    event.preventDefault();
    axios
      .put(`http://localhost:8000/api/pacientes/${id}/`, paciente, config)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
    navigate("/pacientes");
  }

  return (
    <div className="container mt-5">
      <Card>
        <CardHeader tag="h5">Editar Paciente</CardHeader>
        <CardBody>
          {paciente ? (
            <Form
              onSubmit={(e) => {
                handleUpdate(e, id);
              }}
            >
              <FormGroup>
                <Label for="nombres">Nombres</Label>
                <Input
                  id="nombres"
                  name="nombres"
                  placeholder="nombre"
                  type="text"
                  value={paciente.nombres}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="apellidos">Apellidos</Label>
                <Input
                  id="apellidos"
                  name="apellidos"
                  placeholder="apellidos"
                  type="text"
                  value={paciente.apellidos}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="dni">DNI</Label>
                <Input
                  id="dni"
                  name="dni"
                  placeholder="dni"
                  type="text"
                  value={paciente.dni}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="direccion">Direccion</Label>
                <Input
                  id="direccion"
                  name="direccion"
                  placeholder="direccion"
                  type="textarea"
                  value={paciente.direccion}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="telefono">Telefono</Label>
                <Input
                  id="telefono"
                  name="telefono"
                  placeholder="telefono"
                  type="tel"
                  value={paciente.telefono}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="sexo">Sexo</Label>
                <Input
                  id="sexo"
                  name="sexo"
                  placeholder="sexo"
                  type="text"
                  value={paciente.sexo}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="fecha_nacimiento">Fecha de nacimiento</Label>
                <Input
                  id="fecha_nacimiento"
                  name="fecha_nacimiento"
                  placeholder="fecha_nacimiento"
                  type="date"
                  value={paciente.fecha_nacimiento}
                  onChange={handleOnChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="activo">Activo</Label>
                <Input
                  id="activo"
                  name="activo"
                  type="checkbox"
                  checked={paciente.activo ? "checked" : ""}
                  onChange={handleOnChange}
                />
              </FormGroup>

              <Button>Submit</Button>
            </Form>
          ) : (
            "Loading..."
          )}
        </CardBody>
      </Card>
    </div>
  );
}
