import { Button, Table } from "reactstrap";
import { Link, useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";

import axios from "axios";

const PacienteList = () => {
  const [paciente, setPaciente] = useState([]);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  useEffect(() => {
    const fetchPaciente = async () => {
      const res = await axios.get(
        "http://localhost:8000/api/pacientes/",
        config
      );
      console.log(res.data);
      setPaciente(res.data.results);
    };

    fetchPaciente().catch((err) => console.log(err));
  }, []);

  function handleDelete(id) {
    const confirm = window.confirm("Estas seguro de eliminar?");
    if (confirm) {
      axios
        .delete(`http://localhost:8000/api/pacientes/${id}`, config)
        .then((res) => console.log(res.data))
        .catch((err) => console.log(err));
      window.location.replace("/pacientes");
    }
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h1>Lista de Pacientes</h1>
          <div className="form-group text-center p-2">
            <Link
              className="btn btn-success rounded-pill"
              to="/pacientes/agregar"
            >
              Crear
            </Link>
          </div>
          <hr />
          <Table>
            <thead>
              <tr>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>DNI</th>
                <th>Direccion</th>
                <th>Telefono</th>
                <th>Sexo</th>
                <th>Fecha de nacimiento</th>
                <th>Activo</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {paciente
                ? paciente.map((paciente) => (
                    <tr key={paciente.id}>
                      <td>{paciente.nombres}</td>
                      <td>{paciente.apellidos}</td>
                      <td>{paciente.dni}</td>
                      <td>{paciente.direccion}</td>
                      <td>{paciente.telefono}</td>
                      <td>{paciente.sexo}</td>
                      <td>{paciente.fecha_nacimiento}</td>
                      <td>{paciente.activo ? "Si" : "No"}</td>
                      <td>
                        <Link to={`/pacientes/editar/${paciente.id}/`}>
                          <Button className="btn btn-primary rounded-pill">
                            Editar
                          </Button>
                        </Link>

                        <Button
                          className="btn btn-danger rounded-pill"
                          onClick={() => handleDelete(paciente.id)}
                        >
                          Delete
                        </Button>
                      </td>
                    </tr>
                  ))
                : "No hay registros"}
            </tbody>
          </Table>
        </div>
      </div>
    </div>
  );
};

export default PacienteList;
