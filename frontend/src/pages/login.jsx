import React, { useRef } from "react";

import LoginForm from "../components/login-form";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const navigate = useNavigate();
  const ref = useRef(null);

  const login = (values) => {
    console.log(values);
    const urlToken = "http://localhost:8000/api/token/";
    axios
      .post(urlToken, {
        username: values.username,
        password: values.password,
      })
      .then((response) => {
        localStorage.setItem("token", response.data.access);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <div className="container w-75 mt-5 rounded shadow bg-light">
      <div className="row align-items-strech">
        <div className="col d-none col-md-5 col-lg-5 col-xl-6 rounded"></div>
        <div className="col p-5 rounded bg-light">
          <h2 className="fw-bold text-center py-5">Bienvenido</h2>
          <LoginForm onSubmit={login} innerRef={ref} />
          <div className="d-grid">
            <button
              className="btn btn-primary form-control"
              onClick={() => {
                if (ref.current) {
                  ref.current.submitForm();
                  navigate("/");
                }
              }}
            >
              Login
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
