import { AddMedico } from "../components/Medico/AddMedico";
import MedicoList from "../components/Medico/MedicoList";
import { UpdateMedico } from "../components/Medico/UpdateMedico";
import axios from "axios";
import { useState } from "react";

export const Medico = () => {
  const [authorized, setAuthorized] = useState(false);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  axios
    .get("http://127.0.0.1:8000/api/medicos/", config)
    .then((response) => {
      setAuthorized(true);
    })
    .catch((error) => {
      setAuthorized(false);
      localStorage.removeItem("token");
    });

  return (
    <div className="container mt-5">
      {authorized ? (
        <div>
          <MedicoList />
        </div>
      ) : (
        <center>Debes estar autenticado para acceder.</center>
      )}
    </div>
  );
};
