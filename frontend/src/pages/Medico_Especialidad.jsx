import MedicoEspecialidadList from "../components/MedicoEspecialidad/MedicoEspecialidadList";
import axios from "axios";
import { useState } from "react";

export const MedicoEspecialidad = () => {
  const [authorized, setAuthorized] = useState(false);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  axios
    .get("http://127.0.0.1:8000/api/medicos_especialidades/", config)
    .then((response) => {
      setAuthorized(true);
    })
    .catch((error) => {
      setAuthorized(false);
      localStorage.removeItem("token");
    });

  return (
    <div className="container mt-5">
      {authorized ? (
        <div>
          <MedicoEspecialidadList />
        </div>
      ) : (
        <center>Debes estar autenticado para acceder.</center>
      )}
    </div>
  );
};
