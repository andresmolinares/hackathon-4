import { AddEspecialidad } from "../components/Especialidad/AddEspecialidad";
import EspecialidadList from "../components/Especialidad/EspecialidadList";
import { UpdateEspecialidad } from "../components/Especialidad/UpdateEspecialidad";
import axios from "axios";
import { useState } from "react";

export const Especialidad = () => {
  const [authorized, setAuthorized] = useState(false);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  axios
    .get("http://127.0.0.1:8000/api/especialidades/", config)
    .then((response) => {
      setAuthorized(true);
    })
    .catch((error) => {
      setAuthorized(false);
      localStorage.removeItem("token");
    });

  return (
    <div className="container mt-5">
      {authorized ? (
        <div>
          <EspecialidadList />
        </div>
      ) : (
        <center>Debes estar autenticado para acceder.</center>
      )}
    </div>
  );
};
