export function Home() {
  return (
    <div className="container">
      <div className="row mt-5">
        <div className="col-md-12">
          <h1>Home</h1>
          <small>Proyecto Hackaton - Hecho por Andres Molinares</small>
        </div>
      </div>
    </div>
  );
}
