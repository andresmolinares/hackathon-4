import { AddCita } from "../components/Cita/AddCita";
import CitaList from "../components/Cita/CitaList";
import { UpdateCita } from "../components/Cita/UpdateCita";
import axios from "axios";
import { useState } from "react";

export const Cita = () => {
  const [authorized, setAuthorized] = useState(false);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  axios
    .get("http://127.0.0.1:8000/api/citas/", config)
    .then((response) => {
      setAuthorized(true);
    })
    .catch((error) => {
      setAuthorized(false);
      localStorage.removeItem("token");
    });

  return (
    <div className="container mt-5">
      {authorized ? (
        <div>
          <CitaList />
        </div>
      ) : (
        <center>Debes estar autenticado para acceder.</center>
      )}
    </div>
  );
};
