import { AddPaciente } from "../components/Paciente/AddPaciente";
import PacienteList from "../components/Paciente/PacienteList";
import { UpdatePaciente } from "../components/Paciente/UpdatePaciente";
import axios from "axios";
import { useState } from "react";

export const Paciente = () => {
  const [authorized, setAuthorized] = useState(false);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  axios
    .get("http://127.0.0.1:8000/api/pacientes/", config)
    .then((response) => {
      setAuthorized(true);
    })
    .catch((error) => {
      setAuthorized(false);
      localStorage.removeItem("token");
    });

  return (
    <div className="container mt-5">
      {authorized ? (
        <div>
          <PacienteList />
        </div>
      ) : (
        <center>Debes estar autenticado para acceder.</center>
      )}
    </div>
  );
};
