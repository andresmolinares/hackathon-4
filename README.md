# Hackathon 4 - 040622
## Andres Molinares

### Instalacion
### Backend - DRF API
- ```pip install -r requirements.txt```
- ```python manage.py makemigrations```
- ```python manage.py migrate```
- ```python manage.py runserver```

### FrontEnd - REACT
- ```cd frontend```
- ```npm install```
- ```npm run dev```
