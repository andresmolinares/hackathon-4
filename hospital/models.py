from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.
class User(AbstractUser):

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Medico(models.Model):
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    dni = models.CharField(max_length=11)
    direccion = models.TextField()
    correo = models.EmailField(max_length=50)
    telefono = models.CharField(max_length=11)
    sexo = models.CharField(max_length=10)
    numcolegiatura = models.CharField(max_length=50)
    fecha_nacimiento = models.DateField()
    fecha_registro = models.DateField(auto_now=True)
    fecha_modificacion = models.DateField(auto_now_add=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_registro_medico', null=True, blank=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_modificacion_medico', null=True, blank=True)
    activo = models.BooleanField(default=True)
    def __str__(self):
        return self.nombres


class Paciente(models.Model):
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    dni = models.CharField(max_length=11)
    direccion = models.TextField()
    telefono = models.CharField(max_length=11)
    sexo = models.CharField(max_length=10)
    fecha_nacimiento = models.DateField()
    fecha_registro = models.DateField(auto_now=True)
    fecha_modificacion = models.DateField(auto_now_add=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_registro_paciente', null=True, blank=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_modificacion_paciente', null=True, blank=True)
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.nombres


class Cita(models.Model):
    id_medico = models.ForeignKey(Medico, on_delete=models.CASCADE)
    id_paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    inicio_atencion = models.TimeField()
    fin_atencion = models.TimeField()
    estado = models.BooleanField(default=True)
    observaciones = models.TextField()
    activo = models.BooleanField(default=True)
    fecha_registro = models.DateField(auto_now=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_registro_cita', null=True, blank=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_modificacion_cita', null=True, blank=True)
    fecha_modificacion = models.DateField(auto_now_add=True)


    def __str__(self):
        return self.id_medico.nombres


class horarios(models.Model):
    id_medico = models.ForeignKey(Medico, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    inicio_atencion = models.TimeField()
    fin_atencion = models.TimeField()
    activo = models.BooleanField(default=True)
    fecha_registro = models.DateField(auto_now=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_registro_horarios', null=True, blank=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_modificacion_horarios', null=True, blank=True)
    fecha_modificacion = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.id_medico.nombres


class Especialidad(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField()
    activo = models.BooleanField(default=True)
    fecha_registro = models.DateField(auto_now=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_registro_especialidad', null=True, blank=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_modificacion_especialidad', null=True, blank=True)
    fecha_modificacion = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.nombre


class Medicos_Especialidades(models.Model):
    id_medico = models.ForeignKey(Medico, on_delete=models.CASCADE)
    id_especialidad = models.ForeignKey(Especialidad, on_delete=models.CASCADE)
    activo = models.BooleanField(default=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_registro_medico_especialidad', null=True, blank=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_modificacion_medico_especialidad', null=True, blank=True)
    fecha_registro = models.DateField(auto_now=True)
    fecha_modificacion = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.id_medico.nombres
