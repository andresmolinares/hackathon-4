from rest_framework import serializers
from rest_framework.relations import SlugRelatedField

from .models import Medico, Paciente, Cita, Especialidad, Medicos_Especialidades, horarios, User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class MedicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medico
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['usuario_registro'] = user
        validated_data['usuario_modificacion'] = user
        return Medico.objects.create(**validated_data)


class PacienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Paciente
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['usuario_registro'] = user
        validated_data['usuario_modificacion'] = user
        return Paciente.objects.create(**validated_data)


class CitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cita
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['usuario_registro'] = user
        validated_data['usuario_modificacion'] = user
        return Cita.objects.create(**validated_data)


class EspecialidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Especialidad
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['usuario_registro'] = user
        validated_data['usuario_modificacion'] = user
        return Especialidad.objects.create(**validated_data)


class Medicos_EspecialidadesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medicos_Especialidades
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['usuario_registro'] = user
        validated_data['usuario_modificacion'] = user
        return Medicos_Especialidades.objects.create(**validated_data)


class horariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = horarios
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['usuario_registro'] = user
        validated_data['usuario_modificacion'] = user
        return horarios.objects.create(**validated_data)
