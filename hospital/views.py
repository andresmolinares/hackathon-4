from django.shortcuts import render
from .serializers import UserSerializer, PacienteSerializer, CitaSerializer, EspecialidadSerializer, \
    Medicos_EspecialidadesSerializer, horariosSerializer, MedicoSerializer
from rest_framework import viewsets
from .models import User, Medico, Paciente, Cita, Especialidad, Medicos_Especialidades, horarios


# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class MedicoViewSet(viewsets.ModelViewSet):
    queryset = Medico.objects.all()
    serializer_class = MedicoSerializer


class PacienteViewSet(viewsets.ModelViewSet):
    queryset = Paciente.objects.all()
    serializer_class = PacienteSerializer


class CitaViewSet(viewsets.ModelViewSet):
    queryset = Cita.objects.all()
    serializer_class = CitaSerializer


class EspecialidadViewSet(viewsets.ModelViewSet):
    queryset = Especialidad.objects.all()
    serializer_class = EspecialidadSerializer


class Medicos_EspecialidadesViewSet(viewsets.ModelViewSet):
    queryset = Medicos_Especialidades.objects.all()
    serializer_class = Medicos_EspecialidadesSerializer


class horariosViewSet(viewsets.ModelViewSet):
    queryset = horarios.objects.all()
    serializer_class = horariosSerializer
