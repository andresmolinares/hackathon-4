from django.contrib import admin
from .models import Medico, Paciente, Cita, Especialidad, Medicos_Especialidades, horarios


# Register your models here.

class MedicoAdmin(admin.ModelAdmin):
    list = (
        'nombres', 'apellidos', 'dni', 'telefono', 'direccion', 'email', 'activo', 'fecha_registro', 'usuario_registro',
        'fecha_modificacion', 'usuario_modificacion')


admin.site.register(Medico, MedicoAdmin)


class PacienteAdmin(admin.ModelAdmin):
    list = ('nombres', 'apellidos', 'dni', 'telefono', 'direccion', 'email', 'activo', 'fecha_registro',
            'usuario_registro', 'fecha_modificacion', 'usuario_modificacion')


admin.site.register(Paciente, PacienteAdmin)


class CitaAdmin(admin.ModelAdmin):
    list = ('id_medico', 'id_paciente', 'fecha_atencion', 'inicio_atencion', 'fin_atencion', 'estado',
            'observaciones', 'activo', 'fecha_registro', 'usuario_registro', 'fecha_modificacion',
            'usuario_modificacion')


admin.site.register(Cita, CitaAdmin)


class EspecialidadAdmin(admin.ModelAdmin):
    list = ('nombre', 'descripcion', 'activo', 'fecha_registro', 'usuario_registro', 'fecha_modificacion',
            'usuario_modificacion')


admin.site.register(Especialidad, EspecialidadAdmin)


class Medicos_EspecialidadesAdmin(admin.ModelAdmin):
    list = ('id_medico', 'id_especialidad', 'activo', 'fecha_registro', 'usuario_registro', 'fecha_modificacion',
            'usuario_modificacion')


admin.site.register(Medicos_Especialidades, Medicos_EspecialidadesAdmin)


class horariosAdmin(admin.ModelAdmin):
    list = ('id_medico', 'fecha_atencion', 'inicio_atencion', 'fin_atencion', 'activo', 'fecha_registro',
            'usuario_registro', 'fecha_modificacion', 'usuario_modificacion')


admin.site.register(horarios, horariosAdmin)
